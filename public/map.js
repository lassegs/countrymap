var map = L.map('mapid').setView([41.53, 12.30], 2);

function getColor(d) {
    return d > 210 ? '#004c6d' :
           d > 200  ? '#255e7e' :
           d > 180  ? '#3d708f' :
           d > 160  ? '#5383a1' :
           d > 140   ? '#6996b3' :
           d > 120   ? '#7faac6' :
           d > 100   ? '#94bed9' :
           d > 80    ? '#abd2ec' :
	   d > 35    ? '#c1e7ff' :
		       '#cccccc' ;
}	

function style(feature) {
	return {
		fillColor: getColor(feature.properties.dlspeed2),
		fillOpacity: 1,
		color: "#ffffff",
		weight: 1
	};
}


var geojson;

function highlightFeature(e)	{
	var layer = e.target;
	
	layer.setStyle({
		weight: 3,
		color: '#666666'
	});

	if (!L.Browser.ie && ! L.Browser.opera && !L.Browser.edge) {
		layer.bringToFront();
	}
	
        info.update(layer.feature.properties);
}

function resetHighlight(e) {
	geojson.resetStyle(e.target);
	info.update();
}

function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}

function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}

$.getJSON("tm_world_borders_dlspeed.geojson",function(data){
	geojson = L.geoJson(data, {
		style: style,
		onEachFeature: onEachFeature
	}).addTo(map);
});

var info = L.control();

info.onAdd = function (map) {
    this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
    this.update();
    return this._div;
};

// method that we will use to update the control based on feature properties passed
info.update = function (props) {
    this._div.innerHTML = '<h4>2020 Download speeds</h4>' +  (props ?
        '<b>' + props.name + '</b><br />' + props.dlspeed2 + ' mbit/s'
        : 'Hover over a nation');
};

info.addTo(map);


var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
        grades = ['NoData', 35, 80, 100, 120, 140, 160, 180, 200, 210],
        labels = [];

    // loop through our density intervals and generate a label with a colored square for each interval
    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' + getColor(grades[i] + 1) + '"></i> ' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '+');
    }

    return div;
};

legend.addTo(map);


/** L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibGFzc2VndWwiLCJhIjoiQVNxTklPSSJ9.WGPBVU6BcFO8ptFvjWpiDA', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'your.mapbox.access.token'
}).addTo(map); **/
